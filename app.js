const firebase = require("firebase");
const firestore = require("firebase/firestore");
const mqtt = require("mqtt");
const BROKER = "http://localhost:1883";
const TOPIC = "outTopic";
const FIREBASECONFIG = {
  apiKey: "AIzaSyDGroq8m-CzQbaYz7w1OT7R0mGQvbF4lQM",
  authDomain: "carspace-demo-node.firebaseapp.com",
  databaseURL: "https://carspace-demo-node.firebaseio.com",
  projectId: "carspace-demo-node",
  storageBucket: "",
  messagingSenderId: "399207485324",
  appId: "1:399207485324:web:bb056469e07eddc49790a9"
};
var client = mqtt.connect(BROKER);

firebase.initializeApp(FIREBASECONFIG);

client.on("connect", function() {
  try {
    client.subscribe(TOPIC, (err) => {
      if (!err) {
        console.log(`Connection successful: ${BROKER}`);
      }
    });
  } catch (err) {
    console.log(err);
  }
});

try {
  client.on("message", function(topic, message) {
    // message is Buffer
    let currentMessage = message.toString();
    currentMessage = currentMessage.split(":");
    firebase
      .firestore()
      .collection("lots")
      .doc(currentMessage[0])
      .set({ lotNumber: currentMessage[0], lotStatus: currentMessage[1], lastUpdate: Date.now() });
    console.log(currentMessage);
    //client.end();
  });
} catch (err) {
  console.log(err);
}
